---
layout: page
title: Media
---

## Press

- [【速報】「ロボカップアジアパシフィック2021あいち」にて、玉川大学チーム「eR@sers」が部門優勝や上位入賞を多数獲得！](https://bit.ly/3IbvzQU) [2021] - 玉川大学

- [ロボカップ世界大会で玉川大学チームが部門優勝！](https://bit.ly/3yPmPLB) [2021] - 玉川大学

- [「STREAM Hall 2019」の「ロボティクスラボ」オープンでますます加速する玉川大学工学部のロボット研究工学部情報通信工学科・岡田浩之研究室を訪ねて](https://bit.ly/39VpSbI) [2021] - 玉川大学

- [玉川大学eR@sersがロボカップジャパンオープン2020　＠ホーム部門　エデュケーションリーグで優勝！](https://bit.ly/2M4lhdL) [2020] - 玉川大学

- [大学工学部・岡田浩之研究室「eR@sers（イレイサーズ）」](https://bit.ly/38s1ITM) [2019] - 玉川大学

- [玉川学園を探検・調査しよう１](https://bit.ly/38vh2ih) [2019] - 玉川大学

- [ドレクセル大学生と玉川大学生で共同授業を開催。高齢化社会における課題についてまとめ、発表を行いました](https://bit.ly/3iB7dE2) [2019] - 玉川大学

- [先端知能・ロボット研究センター（AIBot研究センター）](https://bit.ly/2C7FHxu) [2019] - 玉川大学

- [Advanced Intelligence & Robotics Research Center (AIBot)](https://bit.ly/2Z1O1b9) [2019] - Tamagawa University


## Video

- [玉川大学　研究所AIBot](https://bit.ly/3pVeIcq) [Video, 2021] - 玉川大学

- [玉川大学　キャンパス紹介](https://bit.ly/3e44cbW) [Video, 2019] - 玉川大学
