---
layout: page
title: People
---

## Faculty

- Prof. Hiroyuki Okada

- Prof. Tetsunari Inamura

- Asst. Prof. Yoshiaki Mizuchi

- Dr. Luis Contreras


## Students

### PhD

### Master

- Arata Sakamaki

- Naoki Oka

- Ryuji Iino

- Yusuke Tanno

### Undergraduates

- Gai Nakatogawa

- Naoya Takenaka

- Haruya Nagata

- So Mochizuki

- Ryohei Kobayashi

- Aiki Ishikawa

- Kanta Kozeni

- Sean Yoshihiro Isoda

- Kyoka Nishimura

<p align="center">
  <img src="{{ '/images/erasers_team.jpg' | relative_url }}" alt="The eR@sers Team in 2021" >
</p>
