---
layout: page
title: Teaching
---

## Online

* [Lessons on Mobile Robot Localization and Kalman Filters](https://bit.ly/3HAiy4l) [Matlab File Exchange] - UNAM, Tamagawa University, 2022

* [Lessons on Mobile Robot Localization and Kalman Filters](https://bit.ly/39ELDz6) [Source Code Repository] - UNAM, Tamagawa University, 2022

* [An introduction to Robot Vision](https://bit.ly/39Tzm7i) [Repository] - eR@sers, Tamagawa University, 2020

* [Robot Localisation: An Introduction](https://bit.ly/374Hn7F) [Video] - RoboCup@Home Education, 2020

* [World Representation Through Artificial Neural Networks](https://bit.ly/3rk3ZbQ) [Video] - RoboCup@Home Education, 2020


## Additional Experience

- [Open Platform Final](https://bit.ly/36MJMDQ) [Video] -  RoboCup@Home Education Online Challenge 2020
