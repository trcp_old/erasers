---
layout: page
title: Research
---

## Publications

### Journal

- Evaluation of an Online Human-Robot Interaction Competition Platform based on Virtual Reality – Case Study in RCAP2021 (2023). Advanced Robotics 37:8, 510-517

- Towards general purpose service robots: World Robot Summit – Partner Robot Challenge (2022). Advanced Robotics, 36:17-18, 812-824

- Sparse-Map: automatic topological map creation via unsupervised learning techniques (2022). Advanced Robotics, 36:17-18, 825-835

- Personal and Domestic Robotics (2022). Encyclopedia of Robotics. Springer, Berlin, Heidelberg

- SIGVerse: A Cloud-Based VR Platform for Research on Multimodal Human-Robot Interaction (2021).Frontiers in Robotics and AI, 8

- What competitions were conducted in the service categories of the World Robot Summit? (2019). Advanced Robotics

- World Robot Summit サービスカテゴリーは何を競ったのか (2019). 日本ロボット学会誌 37.3, pp. 218-223

- ロボカップ@ ホームにおけるミドルウェアの利用―RoboCup2018 Montoreal 大会― (2018). 計測と制御 57.10, pp. 721-724

- Semantic reasoning in service robots using expert systems (2019). Robotics and Autonomous Systems. Vol. 114, pp. 77-92

- A motion planning system for a domestic service robot (2018). SPIIRAS Proceedings. Issue 60. pp. 5-38

### Conference

- A Continuous Integration Based Simulation Environment for Home Support Robot and its Application to RoboCup Competition (2023). IEEE/SICE International Symposium on System Integrations (SII)

- A survey on general-purpose tasks for domestic service robots (2022). The 40-th Annual Conference of the Robotics Society of Japan

- インスタンスセグメンテーション画像を用いた\ヒストグラム均等化手法による物体認識 (2022). The 40-th Annual Conference of the Robotics Society of Japan

- sDSPL - Towards a benchmark for general-purpose task evaluation in domestic service robots (2021). The 39-th Annual Conference of the Robotics Society of Japan

- 公平性と民主性を両立させた RoboCup 用競技シミュレーションシステムの開発 (2021). The 39-th Annual Conference of the Robotics Society of Japan

- ホームサービスロボットによる物体認識のためのアクティブセンシング戦略 (2021). The 39-th Annual Conference of the Robotics Society of Japan

- パーソナルモビリティを活用した人とロボットの協調 (2021). The 39-th Annual Conference of the Robotics Society of Japan

- Learning tools for mobile robot localization using visual landmarks and the extended Kalman filter (2021). In Proceedings of the 24th RoboCup International Symposium

- Generating Reactive Robots’ Behaviors Using Genetic Algorithms (2021). The International Conference on Agents and Artificial Intelligence

- Multimodal human intention-driven robot motion control in collaborative tasks (2020). The 38-th Annual Conference of the Robotics Society of Japan

- 協力の理論 (2020). 人工知能学会全国大会論文集 第 34 回全国大会. 一般社団法人 人工知能学会

- Feature detection using Hidden Markov Models for 3D-visual recognition (2019). The 19th IEEE International Conference on Autonomous Robot Systems and Competitions

- Robot-object interaction strategies for object recognition (2019). The 37-th Annual Conference of the Robotics Society of Japan

- Intelligent flat-and-textureless object manipulation in Service Robots (2018). Workshop "Towards Robots that Exhibit Manipulation Intelligence", IEEE/RSJ IROS2018

- Multimodal feedback for active robot-object interaction (2018). Workshop "Towards Robots that Exhibit Manipulation Intelligence", IEEE/RSJ IROS2018

- Visual feedback for active robot-object interaction (2018). The 36-th Annual Conference of the Robotics Society of Japan

## Videos

### RoboCup atHome Full Competitions

- [RoboCup at Home 2021](https://bit.ly/36fy18e)

- [Mexican Tournament of Robotics 2021](https://bit.ly/3qQfvwI)

### Qualification Videos

- [RoboCup@Home Education 2021 - Team eR@sers Qualification Video](https://bit.ly/2TBX1n9)

- [RoboCup@home DSPL 2020 - Team TamaGoya Qualification Video](https://bit.ly/2C69zKm)

- [RoboCup@home DSPL 2019 - Team eR@sers Qualification Video](https://bit.ly/3eY9N4R)

- [RoboCup@Home DSPL 2018 - Team eR@sers Qualification Video](https://bit.ly/3irzM6Z)

- [RoboCup@Home Education 2020 - Team eR@sers Qualification Video](https://bit.ly/2YZZQ1i)

### Others

- [TamaGoya - Poster, RoboCup 2021](https://bit.ly/3hfw7KU)

- [eR@sers DSPL - Technical Challenge, RoboCup Japan Open 2020](https://bit.ly/3yobLVD)

- [eR@sers Education - Poster, RoboCup Japan Open 2020](https://bit.ly/2TEPsfv)

- [eR@sers Education - Technical Challenge, RoboCup Japan Open 2020](https://bit.ly/3wcdsE8)

- [Robot-Object Interaction with Multimodal Feedback](https://bit.ly/2VKfoUK)

- [Clean-Up Task by Team eR@sers](https://bit.ly/2ZwunD0)

- [Team eR@sers Description Video](https://bit.ly/2C8R1sU)
