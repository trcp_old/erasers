---
layout: page
title: eR@sers
---

Team eR@sers was formed around 2000 to participate in RoboCup 4 legged league. Thereafter, the team joined the @home leage where eR@sers achieved a first place at RoboCup 2008, 2010, second place in RoboCup 2009, 2012, 2017, and third place in RoboCup 2018, and its social robot HSR obtained the @Home Innovation Award in 2016. Furthermore, Team eR@sers was finalist in World Robot Summit (WRS) 2018 and semi-finalist in WRS 2020. Moreover, we won the 1st place in the Flying Robot Challenge, 2nd place in the atHome DSPL and 3rd place in the atHome sDSPL categories; also, we received the Robotics Society of Japan Award for our research on domestic service robots at the RoboCup Asia-Pacific 2021.

Following this tradition, eR@sers @Education Team formed at the end of 2019 after a workshop organised by the World Robot Summit Junior category committee and, since then, we have been developing our own architecture aimed to introduce service robotics to new students avid to participate in the @Home major categories. We have won the First Place in the Education category at the RoboCup Japan Open 2020 and the RoboCup 2021 as well as the Best Education Award for our contribution to the field.

We mainly focus on the adaptability to the environmental changes and on the integration between the sensory-motor data and symbolic representation, utilizing only the neuro-dynamical model. All developed functions can be packed in ROS modules and almost all training data comes from real sources; the system has been tested in real environments.

<p align="center">
  <!--<img src="{{ '/images/erasers_team_2.jpg' | relative_url }}" alt="The eR@sers Team in 2021" >-->
  <img src="{{ '/images/RCAP2021.jpg' | relative_url }}" alt="The eR@sers Team in 2021" >
</p>
